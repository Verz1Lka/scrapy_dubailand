# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import MySQLdb


class DubailandPipeline(object):
    def process_item(self, item, spider):
        query = 'INSERT INTO dubai ('
        for key in item:
            query += key + ","
        query = query[:-1] + ") VALUES ('"
        for key in item:
            query += unicode(item[key]).replace("'", "''").replace('\\', '\\\\') + "','"
        query = query[:-2] + ')'
        try:
            spider.cur.execute(query)
            spider.conn.commit()
        except MySQLdb.Error as e:
            print query
            print e
        return item
