# -*- coding: utf-8 -*-
import scrapy
import datetime
import MySQLdb
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from dubailand.items import DubailandItem


class DubailandGovAeSpider(scrapy.Spider):
    name = "dubailand.gov.ae"
    allowed_domains = ["dubailand.gov.ae"]
    start_urls = (
        'http://www.dubailand.gov.ae/English/RealEstateTransaction/Pages/Daily-Transactions.aspx',
    )
    #conn = MySQLdb.connect(host="localhost", port=3306, user="root", passwd="fxlolpro", db="fx_test", charset='utf8')
    #cur = conn.cursor()
    #cur.execute('SET NAMES UTF8')
    dates = []
    dates_check = []
    item_count = 0

    def __init__(self, start_date=str(datetime.date.today()), end_date=str(datetime.date.today()), **kwargs):
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d').date()
        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
        while start_date <= end_date:
            self.dates.append(start_date)
            delta = datetime.timedelta(days=1)
            start_date += delta
        print [str(dt) for dt in self.dates]
        return

    def spider_closed(self):
        rest_days = open('restDays.txt', 'w')
        for dt in self.dates_check:
            rest_days.write(str(dt) + '\n')
        rest_days.close()

    def parse(self, response):
        txt_field = response.xpath('//input[contains(@name, "txtDate")]/@name').extract()[0].replace('txtDate', '')
        # print txt_field
        radio_field = response.xpath('//input[contains(@name, "radSourse")]/@name').extract()[0]
        # print radio_field
        procs = {11: 'Registration', 13: 'Sale'}
        srcs = {0: 'Established', 1: 'Map'}
        for dt in self.dates:
            for pr in procs:
                for src in srcs:
                    formdata = {txt_field + 'txtDate': dt.strftime('%d/%m/%Y'), txt_field + 'ddlProcedure': str(pr),
                                txt_field + 'btnSearch': 'Search', radio_field: str(src)}
                    rq = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.get_data)
                    rq.meta['proc'] = procs[pr]
                    rq.meta['date'] = str(dt)
                    rq.meta['source'] = srcs[src]
                    yield rq
                    # break
                # break
                    self.dates_check.append(str(dt))
        return
    
    def get_data(self, response):
        # open('out.html', 'w').write(response.body)
        self.dates_check.remove(response.meta['date'])
        for row in response.xpath('//table[contains(@class,"dataTable")]'):
            # row = response.xpath('//table[contains(@id,"grdView")]')[1]
            tp = row.xpath('../preceding-sibling::h3[1]/span/text()').extract_first().split()[1]
            f = 0
            for rw in row.xpath('.//tr[./td]'):
                if len(rw.extract()) < 4:
                    continue
                r = [''.join(r.xpath('./text()').extract()) for r in rw.xpath('./td')]
                # print rw.xpath('./text()').extract()
                itm = DubailandItem()
                itm['tp'] = tp
                itm['data_source'] = response.meta['source']
                itm['proc'] = response.meta['proc']
                itm['date'] = response.meta['date']
                itm['region'] = r[0]
                itm['description'] = r[1]
                if tp == 'Units':
                    itm['bilding'] = r[2]
                    f = 1
                area = r[f + 2].replace(',', '')
                if area:
                    itm['area'] = float(area)
                else:
                    itm['area'] = 0
                itm['totalWorth'] = float(r[f + 3].replace(',', ''))
                itm['sqm'] = float(r[f + 4].replace(',', ''))
                yield itm
                self.item_count += 1
                # break
        print self.item_count
        return
