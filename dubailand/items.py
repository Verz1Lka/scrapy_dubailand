# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class DubailandItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    region = scrapy.Field()
    #region1 = scrapy.Field()
    area = scrapy.Field()
    bilding = scrapy.Field()
    description = scrapy.Field()
    totalWorth = scrapy.Field()
    sqm = scrapy.Field()
    proc = scrapy.Field()
    date = scrapy.Field()
    data_source = scrapy.Field()
    tp = scrapy.Field()
